package com.oliveiradev.springfinance.model.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oliveiradev.springfinance.model.entity.Appointment;
import com.oliveiradev.springfinance.model.enums.AppointmentStatus;
import com.oliveiradev.springfinance.model.enums.AppointmentType;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

	@Query( value = 
			  " select sum(l.value) from Appointment l join l.user u "
			+ " where u.id = :id_user and l.type =:type and l.status = :status group by u " )
	BigDecimal getBalanceByAppointmentTypeAndUserAndStatus(
			@Param("id_user") Long id, 
			@Param("type") AppointmentType type,
			@Param("status") AppointmentStatus status);

	void deleteByDescription(String description);
}