package com.oliveiradev.springfinance.model.enums;

public enum AppointmentType {
	RECEIPT,
	DEBIT
}
