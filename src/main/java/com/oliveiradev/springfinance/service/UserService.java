package com.oliveiradev.springfinance.service;

import java.util.Optional;

import com.oliveiradev.springfinance.model.entity.User;

public interface UserService {
	User authenticate(String email, String password);
	
	User createUser(User user);
	
	void validateEmail(String email);
	
	Optional<User> getUserById(Long id);
	
	void deleteUser(User user);
	
	void deleteUserByEmail(String email);
}
