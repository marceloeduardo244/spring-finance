package com.oliveiradev.springfinance.service.impl;

import java.util.Optional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oliveiradev.springfinance.exceptions.ApplicationPersonalizatedRulesException;
import com.oliveiradev.springfinance.model.entity.Appointment;
import com.oliveiradev.springfinance.model.enums.AppointmentStatus;
import com.oliveiradev.springfinance.model.enums.AppointmentType;
import com.oliveiradev.springfinance.model.repository.AppointmentRepository;
import com.oliveiradev.springfinance.service.AppointmentService;

@Service
public class AppointmentServiceImpl implements AppointmentService {
	
	@Autowired
	AppointmentRepository appointmentRepository;

	@Override
	@Transactional
	public Appointment createAppointment(Appointment appointment) {
		validateAppointment(appointment);
		appointment.setStatus(AppointmentStatus.OPEN);
		return appointmentRepository.save(appointment);
	}

	@Override
	@Transactional
	public Appointment updateAppointment(Appointment appointment) {
		Objects.requireNonNull(appointment.getId());
		validateAppointment(appointment);
		return appointmentRepository.save(appointment);
	}

	@Override
	@Transactional
	public void deleteAppointment(Appointment appointment) {
		Objects.requireNonNull(appointment.getId());
		appointmentRepository.delete(appointment);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Appointment> getListAppointments(Appointment appointmentFilter) {
		Example<Appointment> example = Example.of(appointmentFilter, 
				ExampleMatcher.matching()
				.withIgnoreCase().withStringMatcher(StringMatcher.CONTAINING));
		return appointmentRepository.findAll(example);
	}

	@Override
	public void updateStatus(Appointment appointment, AppointmentStatus status) {
		appointment.setStatus(status);
		updateAppointment(appointment);
	}

	@Override
	public void validateAppointment(Appointment appointment) {
		
		if (appointment.getDescription() == null || appointment.getDescription().trim().equals("")) {
			throw new ApplicationPersonalizatedRulesException("Inform a valid description");
		}
		
		if (appointment.getMonth() == null 
				|| appointment.getMonth() < 1
				|| appointment.getMonth() > 12) {
			throw new ApplicationPersonalizatedRulesException("Inform a valid month");
		}
		
		if (appointment.getYear() == null 
				|| appointment.getYear().toString().length() != 4) {
			throw new ApplicationPersonalizatedRulesException("Inform a valid year");
		}
		
		if (appointment.getUser() == null ||
				appointment.getUser().getId() == null) {
			throw new ApplicationPersonalizatedRulesException("Inform a valid user");
		}
		
		if (appointment.getValue() == null ||
				appointment.getValue().compareTo(BigDecimal.ZERO) < 1) {
			throw new ApplicationPersonalizatedRulesException("Inform a valid value");
		}
		
		if (appointment.getType() == null) {
			throw new ApplicationPersonalizatedRulesException("Inform a valid appointment type");
		}
		
	}
	
	@Override
	public Optional<Appointment> getAppointmentById(Long id) {
		return appointmentRepository.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public BigDecimal getBalanceByUserId(Long id) {
		BigDecimal receipts = appointmentRepository.getBalanceByAppointmentTypeAndUserAndStatus(id, AppointmentType.RECEIPT, AppointmentStatus.CLOSED);
		BigDecimal debits = appointmentRepository.getBalanceByAppointmentTypeAndUserAndStatus(id, AppointmentType.DEBIT, AppointmentStatus.CLOSED);
		
		if(receipts == null) {
			receipts = BigDecimal.ZERO;
		}
		
		if(debits == null) {
			debits = BigDecimal.ZERO;
		}
		
		return receipts.subtract(debits);
	}

	@Override
	@Transactional
	public void deleteByDescription(String description) {
		appointmentRepository.deleteByDescription(description);
	}

}
