package com.oliveiradev.springfinance.api.controller;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oliveiradev.springfinance.api.dto.TokenDTO;
import com.oliveiradev.springfinance.api.dto.UserDTO;
import com.oliveiradev.springfinance.exceptions.ApplicationPersonalizatedRulesException;
import com.oliveiradev.springfinance.exceptions.AuthenticationErrorException;
import com.oliveiradev.springfinance.model.entity.User;
import com.oliveiradev.springfinance.service.AppointmentService;
import com.oliveiradev.springfinance.service.JwtService;
import com.oliveiradev.springfinance.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	AppointmentService appointmentService;
	
	@Autowired
	JwtService jwtService;
	
	@PostMapping
	public ResponseEntity<Object> save(@RequestBody UserDTO dto) {
		
		User user = new User(dto.getName(), dto.getEmail(), dto.getPassword());
		
		try {
			User createdUser = userService.createUser(user);
			return new ResponseEntity<Object>(HttpStatus.CREATED);
		} catch (ApplicationPersonalizatedRulesException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}
	
	@PostMapping("/auth")
	public ResponseEntity<?> autenticar( @RequestBody UserDTO dto ) {
		try {
			User authUser = userService.authenticate(dto.getEmail(), dto.getPassword());
			String token = jwtService.generateToken(authUser);
			TokenDTO tokenDTO = new TokenDTO(authUser.getName(), token);
			
			return ResponseEntity.ok(tokenDTO);
		} catch (AuthenticationErrorException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}
	
	@GetMapping("{id}/balance")
	public ResponseEntity obterSaldo( @PathVariable("id") Long id ) {
		Optional<User> usuario = userService.getUserById(id);
		
		if(!usuario.isPresent()) {
			return new ResponseEntity( HttpStatus.NOT_FOUND );
		}
		
		BigDecimal saldo = appointmentService.getBalanceByUserId(id);
		return ResponseEntity.ok(saldo);
	}
}
