package com.oliveiradev.springfinance.api.dto;

import java.util.Objects;

public class UserDTO {
	private String email;
	private String name;
	private String password;

	public UserDTO() {
	}

	public UserDTO(String email, String nome, String senha) {
		super();
		this.email = email;
		this.name = nome;
		this.password = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String nome) {
		this.name = nome;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String senha) {
		this.password = senha;
	}

	@Override
	public int hashCode() {
		return Objects.hash(email, name, password);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDTO other = (UserDTO) obj;
		return Objects.equals(email, other.email) && Objects.equals(name, other.name)
				&& Objects.equals(password, other.password);
	}

	@Override
	public String toString() {
		return "UserDTO [email=" + email + ", name=" + name + ", password=" + password + "]";
	}

}
