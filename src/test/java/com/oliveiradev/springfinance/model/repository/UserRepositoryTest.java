package com.oliveiradev.springfinance.model.repository;

import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.oliveiradev.springfinance.model.entity.User;
import com.oliveiradev.springfinance.service.AppointmentService;
import com.oliveiradev.springfinance.service.UserService;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class UserRepositoryTest {

	@Autowired
	UserRepository repository;
	
	@Autowired
	UserService userService;
	
	@Autowired
	AppointmentService appointmentService;
	
    @BeforeAll
    public void init() {
    	appointmentService.deleteByDescription("test_integration_587");
    	userService.deleteUserByEmail("test_integration_587@email.com");
    }

    @AfterAll
    public void teardown() {
    	appointmentService.deleteByDescription("test_integration_587");
    	userService.deleteUserByEmail("test_integration_587@email.com");
    }
	
	public User createMockUser() {
		userService.deleteUserByEmail("test_integration_587@email.com");
		User user = new User("test_integration_587", "test_integration_587@email.com", "123456");
		return user;
	}
	
	@Test
	public void mustVerifyIfEmailExist() {
		// scene
		User user = createMockUser();
		repository.save(user);
		// action
		boolean exist = repository.existsByEmail("test_integration_587@email.com");
		
		// verify
		Assertions.assertTrue(exist);
		
		userService.deleteUser(user);
	}
	
	@Test
	public void mustVerifyIfEmailNotExist() {
		// scene
		User user = createMockUser();
		repository.save(user);
		// action
		boolean exist = repository.existsByEmail("test_integration_587B@email.com");
		
		// verify
		Assertions.assertFalse(exist);
		
		userService.deleteUser(user);
	}
	
	@Test
	public void mustGetExistantUserByEMail() {
		User user = createMockUser();
		repository.save(user);
		
		Optional<User> possibleUserSearched = repository.findByEmail(user.getEmail());
		
		Assertions.assertTrue(possibleUserSearched.isPresent());
		
		userService.deleteUser(user);
	}
	
	@Test
	public void mustNotGetUserByUnexistantEMail() {
		Optional<User> possibleUserSearched = repository.findByEmail("unexistant_mail875@email.com");
		
		Assertions.assertFalse(possibleUserSearched.isPresent());
	}
}
