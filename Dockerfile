FROM maven:3.6.3-jdk-8-slim AS build
WORKDIR /app

COPY pom.xml .
RUN mvn -e -B dependency:resolve
COPY src/ /app/src/
RUN mvn -e -B package -DskipTests -Dflyway.skip=true

FROM openjdk:8-jdk-slim
ARG JAR_FILE=/app/target/*.jar
COPY --from=build ${JAR_FILE} app.jar

ENV DATABASE_URL -
ENV POSTGRES_PASSWORD -
ENV POSTGRES_USER -
# This container exposes port 8080 to the outside world
EXPOSE 8080

ENTRYPOINT ["java","-jar","/app.jar"]