
postgres:
	docker run --name postgres -p 5432:5432 -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=spring_finance -d postgres:14-alpine

pgadmin:
	docker run --name pgadmin -p 80:80 -e PGADMIN_DEFAULT_EMAIL=marcelo@gmail.com -e PGADMIN_DEFAULT_PASSWORD=SuperSecret -d dpage/pgadmin4
	
run_app_locally:
	docker run --name spring_finance -p 8080:8080 -e DATABASE_URL=jdbc:postgresql://localhost:5432/spring_finance -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e EXPIRATION_TIME=30 -e SECRET_KEY=g5gd5g15dg1d5sg15ds1g5d1z1g45 -d marcelosilva404/spring-finance:1.0